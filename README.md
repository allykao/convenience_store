# vue-project

Vue.js實作Google Map API地圖、地標、訊息視窗，並應用於台中市超商

## 執行專案

- npm install
- npm run dev

## 資料來源

- 政府資料開放平臺(https://data.gov.tw/dataset/32086)